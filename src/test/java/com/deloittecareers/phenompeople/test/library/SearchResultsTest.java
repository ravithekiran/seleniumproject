package com.deloittecareers.phenompeople.test.library;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsTest {

	@FindBy(css = ".job-title")
	List<WebElement> jobTitles;
	
	@FindBy(css = ".job-category")
	List<WebElement> jobCategories;
	
	@FindBy(css = ".job-post-date")
	List<WebElement> jobPostedDate;
	
	@FindBy(css = ".job-loacation")
	List<WebElement> jobCityStCtry;
	
	@FindBy(css = ".cc-multi-locations-link.collapsed")
	List<WebElement> jobCityStCtryMulti;
	
	@FindBy(id = "sortselectt")
	WebElement sortDrpDwn;
	
	@FindBy(id = "#addedtags")
	WebElement selTags;
	
	@FindBy(css = ".tag.city>span")
	List<WebElement> cityTags;
	
	@FindBy(id = ".tag.state>span")
	List<WebElement> stateTags;
	
	@FindBy(css = ".tag.type>span")
	List<WebElement> jobTypeTag;
	
	@FindBy(id = ".tag.category>span")
	List<WebElement> catTags;
	
	@FindBy(id = ".tag.country>span")
	List<WebElement> ctryTags;
	
	@FindBy(css = ".cc-jobs-list-count.search-top-count")
	WebElement updatingJobs;
	


		
}
