package com.deloittecareers.phenompeople.test.deloitte;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;



public class FooterTests {

	WebDriver driver = new FirefoxDriver();
	//String Path = "E:\\FooterLinkTitles.xlsx";

	@Test
	public void gettingLinkNamesTitle() throws Exception {
		driver.get("https://deloitte-qa.imomentous.co/");
		List<WebElement> footerLinksList = driver.findElements(By.cssSelector(".footer-col>li>a"));
		// FileInputStream ExcelFile = new FileInputStream(Path);
		// Workbook wbook=new HSSFWorkbook(ExcelFile);
		// Sheet wSheet = (Sheet) wbook.getSheet("Sheet1");
		// int rowCount =
		// guru99Sheet.getLastRowNum()-guru99Sheet.getFirstRowNum();
		// for (int i = 0; i < rowCount+1; i++) {
		// Row row = guru99Sheet.getRow(i);
		// for (int j = 0; j < row.getLastCellNum(); j++) {
		// System.out.print(row.getCell(j).getStringCellValue()+"|| ");
		//
		// }
		int size = footerLinksList.size();
		String[][] titleLink = new String[size][2];

		for (WebElement links : footerLinksList) {
			String mainPage = driver.getWindowHandle();
			for (int row = 0; row < size; row++) {
				String link = links.getText();
				
				driver.findElement(By.linkText(link)).click();
				Thread.sleep(5000);
				for (String handles : driver.getWindowHandles()) {
					driver.switchTo().window(handles);
				}
					String title = driver.getTitle();
					System.out.println(titleLink);
					System.out.println("hi");
					titleLink[row][1] = link;
					System.out.println("hi");
					
					titleLink[row][2] = title;
					System.out.println(titleLink);
					driver.close();
				
			}
			driver.switchTo().window(mainPage);
			System.out.println(titleLink);
		}
		// for(int i=0;i<footerLinksList.size();i++)
		// {
		// //String link=links.getText();
		// String mainPage=driver.getWindowHandle();
		// driver.findElement(By.linkText(link)).click();
		// for(String handles:driver.getWindowHandles())
		// {
		// String title=driver.getTitle();
		//
		//
		// }
		// }

	}
	
	@Test
	public void validatingTitles() throws InterruptedException
	{
		driver.get("https://deloitte-qa.imomentous.co/");
		List<WebElement> footerLinksListt = driver.findElements(By.cssSelector(".footer-col>li>a"));
		int sizet = footerLinksListt.size();
	String[][] titleLink = new String[sizet][2];
		for (WebElement links : footerLinksListt) {
			String mainPage = driver.getWindowHandle();
			for (int row = 0; row < sizet; row++) {
			String link = links.getText();
				driver.findElement(By.linkText(link)).click();
				Thread.sleep(5000);
			for (String handles : driver.getWindowHandles()) {
					driver.switchTo().window(handles);
					}
			
			String ExpTitle=driver.getTitle();
			//for(array 2D)
			//{
			if(link.equalsIgnoreCase("array[0]"))
			{
					String actualTitle="abc";//array index get title
					try
					{
						Assert.assertEquals(ExpTitle, actualTitle);
						System.out.println(" page tiles  matching"+link);
					}
					catch(Throwable e)
					{
						System.out.println("failed page tiles not matching"+link);
					}
				
					driver.close();
				}
			}
			driver.switchTo().window(mainPage);
			System.out.println(titleLink);
		
	}
	}
}

