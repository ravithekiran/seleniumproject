package com.deloittecareers.phenompeople.test.deloitte;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;

public class Delloitte {
	
	static WebDriver driver=new FirefoxDriver();
	static WebDriverWait wait = new WebDriverWait(driver, 20);
	static int counter=0;
	static WebElement title;
	public static void main(String[] args) throws InterruptedException{
		
		driver.get("http://deloitteg-qa.imomentous.co/us/en/Finance-jobs?rtype=1");
		driver.manage().window().maximize();
		JavascriptExecutor jsx=(JavascriptExecutor)driver;
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//WebElement insearch=driver.findElement(By.id("sub_search_textbox"));
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//driver.get("http://deloitteg-dev.imomentous.co/us/en/Administrative-and-Support-Services-jobs");
	/*	driver.manage().window().maximize();
		String jobcattag=(driver.findElement(By.cssSelector(".cc-jobs-list-count.search-top-count")).getText());
		System.out.println(jobcattag);
		//removing jobs word and rextracting number
		double jobnum=Double.parseDouble((jobcattag.substring(0, jobcattag.length()-5)));
		System.out.println(jobnum);
		double pages=jobnum/50;
		System.out.println(pages);//1.12
		int pagenum=(int) Math.round(pages);
		System.out.println(pagenum);//1
		if(pagenum<pages)
		{
		//pages=(int)pages+1;
		pagenum=(int)pages+1;
		
		}
		System.out.println("Page numbers:"+pagenum);
		int a=(pagenum-1)*50;
		String b=Integer.toString(a);
		//String b=a+"";
		System.out.println(b);
		String lastpagel="&max=50";
		String lastpagef="/job/searchResults?offset="+b+"&max=50";
		
		//String lp=lastpagef+a+lastpagel;a[class=step][href="/job/searchResults?offset=50&max=50"]
		String lastpage=driver.findElement(By.cssSelector("a[class='step'][href='"+lastpagef+"']")).getText().trim().substring(9).replace("\n", "");
		System.out.print("Desktop Page numbers:"+lastpage);*/
		
		//driver.findElement(By.xpath(".//*[@id='refineaccordion']/div[2]/div[1]/a")).click();
				//cssSelector("a[href='#city_0']")).click();
		List<WebElement> facetCity=new ArrayList<WebElement>();
		facetCity=driver.findElements(By.cssSelector("ul[id='state']>li>label>input[type='checkbox']"));
		int facetccount=facetCity.size();
		Thread.sleep(3000);
		 Random randomGenerator = new Random();
		 for (int idx = 1; idx <= 5; idx++){
			 facetCity=driver.findElements(By.cssSelector("ul[id='state']>li>label>input[type='checkbox']"));
		      int randomInt = randomGenerator.nextInt(facetccount);
		      System.out.println("Generated : " + randomInt);
		     /* Coordinates coordinate = ((Locatable)facetCity.get(randomInt)).getCoordinates();
				coordinate.inViewPort();*/
				//jsx.executeScript("window.scrollTo(0,400)", "");
				jsx.executeScript("arguments[0].click()", facetCity.get(randomInt));
		      //facetCity.get(randomInt).click();
		      Thread.sleep(3000);
		    }
	//	"/job/searchResults?offset=150&max=50"

		//srcListdates.equals(destListdates);
		
		//driver.findElement(By.id("keywords")).click();
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//driver.findElement(By.xpath(".//*[@id='searchcategories']/ul[1]/li[5]/a")).click();
		/*String cityfpath=".//*[@id='city']/li[";
		String citybpath="]/label/span[1]";*/
		//driver.findElement(By.xpath(".//*[@id='refineaccordion']/div[2]/div[1]/a")).click();
		
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//String catgry=driver.findElement(By.cssSelector(".tag.category>span")).getText();
		/*System.out.println("hello");
		//driver.findElement(By.xpath(".//*[@id='cc-multi-locations']/a")).click();
		driver.findElement(By.xpath(".//*[@id='cc-multi-locations']/a")).sendKeys(Keys.ENTER);*/
		//.//*[@id='cc-multi-locations']/a
		/*driver.findElement(By.cssSelector("a[class='cc-multi-locations-link collapsed'][data-target='#accDELOA004X1619']")).click();
		System.out.println("hello");
		driver.findElement(By.cssSelector("a[class='cc-multi-locations-link collapsed'][data-target='#accDELOA004X1619']")).click();*/
		/*driver.get("http://deloitteg-dev.imomentous.co/us/en/Finance-jobs");

		List<WebElement> multi= driver.findElements(By.cssSelector(".cc-multi-locations>a"));
		for(WebElement mutiClick:multi)
		{
			String a=mutiClick.getText();
			driver.findElement(By.linkText(a)).click();
			//sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			for(int i=0;i<2;i++)
			{
				((WebElement) driver.findElements(By.cssSelector("cc-multi-locations-link collapsed"))).sendKeys(Keys.ENTER);
			}
		}
		List<WebElement> locs=driver.findElements(By.cssSelector("#accDELOA004X12462>li"));
		for(WebElement mutiLocs:locs)
		{
			String[] location=(mutiLocs.getText()).split(",");
			System.out.println(location[0]+" "+location[1]+" "+location[2]);
		}*/
//		WebDriverWait wait = new WebDriverWait(driver, 20);
//	for(int i=32;i<34;i++){
//		
		//wait.until(ExpectedConditions.elementToBeSelected(By.xpath(cityfpath+i+citybpath)));
	//	WebElement element=driver.findElement(By.xpath(cityfpath+i+citybpath));
				//By.xpath(".//*[@id='city_0']/div[2]/div[1]"));
				//cityfpath+i+citybpath));
				//driver.findElement(By.xpath(".//*[@id='footer-section']/div/div[1]/div[1]/div[2]/div[1]/ul/li[11]/a"));
//		Point loc=element.getLocation();
//		int xcord=loc.getX();
//		int ycord=loc.getY();
		//WebElement element = driver.findElement(By.xpath(�xpah��));
		/*Coordinates coordinate = ((Locatable)element).getCoordinates();
		coordinate.inViewPort();
//		WebElement element1=driver.findElement(By.xpath(
//		Coordinates coordinate1 = ((Locatable)element1).getCoordinates();
//		coordinate1.inViewPort();
			jsx.executeScript("window.scrollTo(0,200)", "");	
			driver.findElement(By.xpath(cityfpath+i+citybpath)).click();*/
			
		//driver.findElement(By.xpath(cityfpath+"33"+citybpath)).click();
//		System.out.println(xcord+" "+ycord);
//		Actions builder = new Actions(driver);
//		builder.moveToElement(element, xcord, ycord).click().build().perform();
//				//builder.moveToElement(knownElement, 10, 25)
//				//.build();
//		//abc.perform();
//		wait.until(ExpectedConditions.visibilityOf(element));
//		element.click();
//		builder.click().build();
//		abc.perform();
//		actions.moveToElement(element);
//		actions.perform();
		//jsx.executeScript("arguments[0].scrollIntoView();", element);
	//driver.findElement(By.xpath(cityfpath+"33"+citybpath)).click();
		
		}

	
//		List<WebElement> cities= driver.findElements(By.xpath("//li[@class='list-group-item city']/label[@class='checkbox']"));
//				//+ "//li[@class='list-group-item city']/label[@class='checkbox']/input[@class='active']"));
//		System.out.println(cities.size());
//		driver.findElement(By.xpath(".//*[@id='refineaccordion']/div[2]/div[1]/a")).click();
//		for(WebElement pcity:cities)
//		{
//			pcity.click();
//			Thread.sleep(1000);

	
			
			//System.out.println(pcity).getAttribute("value");
			//.getAttribute("value")...........//li[@class='list-group-item city']/label[@class='checkbox']/input[@class='active']
	public static WebElement RefreshObject(By locator)
	{
		//counter=counter+1;
		return driver.findElement(locator);
	}		
}
	
	

