package com.deloittecareers.phenompeople.test.deloitte;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.deloittecareers.phenompeople.test.pages.HeaderPage;

public class HeaderTests {

	String studentTitle="Students Jobs | Students Jobs in Deloitte";
	String experienceTitle="Experienced Professionals Jobs | Experienced Professionals Jobs in Deloitte";
	String jobCartTitle="Job cart";
	String actTitle;
	String ExpTitle;
	WebDriver driver = new FirefoxDriver();
	HeaderPage hp=new HeaderPage();
	@Test
	public void studentPage()
	{
		hp.clickingStudentsLink();
		ExpTitle=driver.getTitle();
		try
		{
			Assert.assertEquals(ExpTitle, actTitle);
			System.out.println(" page tiles  matching");
		}
		catch(Throwable e)
		{
			System.out.println("failed page tiles not matching");
		}
	}
	public void experiencePage()
	{	
		hp.clickingExperiencedLink();
		ExpTitle=driver.getTitle();
		try
		{
			Assert.assertEquals(ExpTitle, actTitle);
			System.out.println(" page tiles  matching");
		}
		catch(Throwable e)
		{
			System.out.println("failed page tiles not matching");
		}
	}
	public void jobcartPage()
	{	
		hp.clickingJobCartLink();
		ExpTitle=driver.getTitle();
		try
		{
			Assert.assertEquals(ExpTitle, actTitle);
			System.out.println(" page tiles  matching");
		}
		catch(Throwable e)
		{
			System.out.println("failed page tiles not matching");
		}
	}
	public void careersHomePage()
	{
		hp.clickingCareersHomeLink();
		ExpTitle=driver.getTitle();
		try
		{
			Assert.assertEquals(ExpTitle, actTitle);
			System.out.println(" page tiles  matching");
		}
		catch(Throwable e)
		{
			System.out.println("failed page tiles not matching");
		}
	}
	public void searchBoxClick()
	{
		hp.clickingSearchBox();
		try
		{
			String expValue="Job Categories";
			String actValue=driver.findElement(By.cssSelector("#searchcategories>h3")).getText();
			Assert.assertEquals(actValue, expValue);
			System.out.println("popup menu loaded");
			//gettinging popupmenu data and validating for al[habetical order
			List<WebElement> searchBoxCategories=driver.findElements(By.cssSelector(".search-categori-list>li>a"));
			for(String searchBoxClickItems:searchBoxCategories)
			{
				
			}
		}
		catch(Throwable e)
		{
			System.out.println("popup menu failed to load");
		}
	}
}
