package com.deloittecareers.phenompeople.test.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FooterPage {

	//List<String> linkString1=new ArrayList<String>();
	@FindBy(css=".footer-col>li>a")
	List<WebElement> footerLinksList;
	
	public List footerLinks()
	{
		
		return footerLinksList;
	}
	
}
