package com.deloittecareers.phenompeople.test.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CategoryPage {

	@FindBy(css = ".no-click-close>a")
	List<WebElement> categoryLinks;
}
