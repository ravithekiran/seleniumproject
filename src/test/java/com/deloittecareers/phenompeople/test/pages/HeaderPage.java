package com.deloittecareers.phenompeople.test.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderPage {

	@FindBy(linkText = "Careers home")
	WebElement careerHomeLink;

	@FindBy(linkText = "Students")
	WebElement studentsLink;

	@FindBy(linkText = "Experienced")
	WebElement experiencedLink;

	@FindBy(linkText = "Job cart ")
	WebElement jobCartLink;

	@FindBy(linkText = "Careers ")
	WebElement careersCategoryLink;

	@FindBy(id = "keywords")
	WebElement searchBox;

	@FindBy(css =".btn.btn-primary.btn-lg.search-submit.disable")
	WebElement searchBoxBtn;
	
	@FindBy(css = ".search-categori-list>li>a")
	List<WebElement> onClickCategories;

	@FindBy(id = ".fa.fa-close")
	WebElement btnClearSearchContent;

	public void clickingCareersHomeLink() {
		careerHomeLink.click();
	}

	public void clickingStudentsLink() {
		studentsLink.click();
	}

	public void clickingExperiencedLink() {
		experiencedLink.click();
	}

	public void clickingCareersCategoryLink() {
		careersCategoryLink.click();
	}

	public void clickingJobCartLink() {
		careerHomeLink.click();
	}

	public void clickingSearchBox() {
		searchBox.click();
	}

	public void clickingSearchClearBtn() {
		btnClearSearchContent.click();
	}
}
