package com.deloittecareers.phenompeople.test.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	public HomePage(WebDriver driver) {
		//super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="cookieaccept")
	WebElement cookieLink;
	/*
	@FindBy(id=".opportunities-link>div>div")
	List<WebElement> firstCategoriesContent;
	
	@FindBy(id=".opportunities-link>div>div")
	List<WebElement> laterCategoriesContent;*/
	
	@FindBy(id=".linkedin-login-btn")
	WebElement linkedinBtn;
	
	@FindBy(id = ".no-click-close>a")
	List<WebElement> categoryLinks;
	
	public void clickingCookieLink()
	{
		cookieLink.click();
	}
	public void clickingLinkedinBtn()
	{
		linkedinBtn.click();
	}
	
		/*@FindBy(linkText = "Home")
		WebElement homeFooterLink;

		@FindBy(linkText = " About us")
		WebElement aboutUsLink;

		@FindBy(linkText = "Experienced")
		WebElement experiencedLink;

		@FindBy(linkText = "Job cart ")
		WebElement jobCartLink;

		@FindBy(linkText = "Careers ")
		WebElement careersLink;
		 */
		
	
}
